class Person {
    constructor(name, gender, origin) {
        this.name = name;
        this.gender = gender;
        this.origin = origin;
    }
    sayHello() {
        console.log(
            `Hallo nama saya ${this.name} saya berasal dari ${this.origin}`
        );
    }
}

class Admin extends Person {
    constructor(name, gender, origin) {
        super(name, gender, origin);
    }

    sayHello() {
        console.log(`Hallo saya ${this.name}. Mau booking kamar hotel? `);
    }

    checkInVisitor(visitor, room) {
        if (!room.reservationStatus) {
            visitor.bookedRoom.push(room);
            room.setReservationStatus(true);
            console.log(visitor);
            // console.log(room);
        } else {
            console.log("Kamar sudah di booking orang lain");
        }
    }

    checkOutVisitor(visitor, room) {
        let findIndexOfTheRoom = visitor.bookedRoom.indexOf(room);
        if (findIndexOfTheRoom >= 0) {
            visitor.bookedRoom.remove(findIndexOfTheRoom);
            room.setReservationStatus(false);
            console.log(visitor);
        } else {
            console.log(
                `Kamar nomor ${room.roomID} belum dibooking sebelumnya`
            );
        }
    }
}

class Visitor extends Person {
    constructor(name, gender, origin) {
        super(name, gender, origin);
        this.bookedRoom = [];
    }
}

class Room {
    constructor(roomID) {
        this.roomID = roomID;
        this.reservationStatus = false;
    }
    setReservationStatus(value) {
        this.reservationStatus = value;
    }
}

class RoomType1 extends Room {
    constructor(roomID) {
        super(roomID);
        this.price = 300000;
        this.amenities = [
            "King Bed",
            "WiFi",
            "AC",
            "TV",
            "Kitchen",
            "Free Parking",
        ];
    }
}

class RoomType2 extends Room {
    constructor(roomID) {
        super(roomID);
        this.price = 200000;
        this.amenities = ["Queen Bed", "WiFi", "AC", "TV"];
    }
}

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

let putri = new Admin("Putri", "Perempuan", "Jogja");
let rafi = new Visitor("Rafi", "Laki-laki", "Padang");
let alfian = new Visitor("Alfian", "Laki-laki", "Bandung");

let room1 = new RoomType1(1);
let room2 = new RoomType1(2);
let room3 = new RoomType2(3);
let room4 = new RoomType2(4);


putri.sayHello();

